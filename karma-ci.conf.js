module.exports = function(config) {
	require("./karma.conf")(config);
	config.set({

		// start these browsers
		// available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['MyHeadlessChrome'],
        singleRun: true,

        customLaunchers: {
         MyHeadlessChrome: {
         base: 'ChromeHeadless',
            flags: [
              '--disable-translate', 
              '--disable-extensions', 
            '--remote-debugging-port=9223', 
          '--no-sandbox']
        },
     },
	});
};
